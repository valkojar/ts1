package archive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

public class PurchasesArchiveTest {
    PurchasesArchive purchasesArchive;

    HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchiveInput;
    ArrayList<Order> orderArchiveInput;

    StandardItem refItemInput;
    ItemPurchaseArchiveEntry itemPurchaseArchiveEntryInput;

    ArrayList<Item> itemsInput;
    ShoppingCart cartInput;
    Order orderInput;

    int INPUT_ID_1 = 595;
    int INPUT_ID_2 = 569;

    String INPUT_NAME_1 = "Samsung Galaxy A10";
    String INPUT_NAME_2 = "Sony Xperia T";

    float INPUT_PRICE_1 = 3290;
    float INPUT_PRICE_2 = 1815;

    String INPUT_CATEGORY_1 = "Electronics";
    String INPUT_CATEGORY_2 = "Electronics";

    int INPUT_LOYALTY_1 = 5;
    int INPUT_LOYALTY_2 = 3;

    String INPUT_CUSTOMERNAME = "Jaroslav Valkoun";
    String INPUT_CUSTOMERADDRESS = "Klokočná 101";
    int INPUT_STATE = 1;

    @BeforeEach
    public void setup(){
        refItemInput = new StandardItem(INPUT_ID_1, INPUT_NAME_1, INPUT_PRICE_1, INPUT_CATEGORY_1, INPUT_LOYALTY_1);
        itemPurchaseArchiveEntryInput = new ItemPurchaseArchiveEntry(refItemInput);

        itemPurchaseArchiveInput = new HashMap();
        itemPurchaseArchiveInput.put(0, itemPurchaseArchiveEntryInput);

        itemsInput = new ArrayList<>();

        itemsInput.add(new StandardItem(INPUT_ID_1, INPUT_NAME_1, INPUT_PRICE_1, INPUT_CATEGORY_1, INPUT_LOYALTY_1));
        itemsInput.add(new StandardItem(INPUT_ID_2, INPUT_NAME_2, INPUT_PRICE_2, INPUT_CATEGORY_2, INPUT_LOYALTY_2));

        cartInput = new ShoppingCart(itemsInput);
        orderInput = new Order(cartInput, INPUT_CUSTOMERNAME, INPUT_CUSTOMERADDRESS, INPUT_STATE);

        orderArchiveInput = new ArrayList();

        purchasesArchive = new PurchasesArchive(itemPurchaseArchiveInput,orderArchiveInput);
    }

    @Test
    public void ItemPurchaseArchiveEntryConstructorTest(){
        refItemInput = new StandardItem(INPUT_ID_1, INPUT_NAME_1, INPUT_PRICE_1, INPUT_CATEGORY_1, INPUT_LOYALTY_1);
        itemPurchaseArchiveEntryInput = new ItemPurchaseArchiveEntry(refItemInput);

        Assertions.assertEquals(refItemInput, itemPurchaseArchiveEntryInput.getRefItem());
        Assertions.assertEquals(1, itemPurchaseArchiveEntryInput.getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void PrintItemPurchaseStatisticsTest(){
        ByteArrayOutputStream resultPrint = new ByteArrayOutputStream();
        System.setOut(new PrintStream(resultPrint));

        String expectedOutput = "ITEM PURCHASE STATISTICS:\r\n" +
                "ITEM  Item   ID " + INPUT_ID_1 + "   NAME " + INPUT_NAME_1 + "   CATEGORY "+ INPUT_CATEGORY_1 +
                "   PRICE " + INPUT_PRICE_1 + "   LOYALTY POINTS " + INPUT_LOYALTY_1 + "   HAS BEEN SOLD 1 TIMES\r\n";

        purchasesArchive.printItemPurchaseStatistics();

        Assertions.assertEquals(expectedOutput, resultPrint.toString());
    }

    @Test
    public void GetHowManyTimesHasBeenItemSoldTest(){
        int expectedResult = 0;
        Assertions.assertEquals(expectedResult, purchasesArchive.getHowManyTimesHasBeenItemSold(refItemInput));
    }

    @Test
    public void PutOrderToPurchasesArchiveTest(){
        purchasesArchive.putOrderToPurchasesArchive(orderInput);
        Assertions.assertEquals(orderInput, purchasesArchive.getOrderByKey(0));
    }
}
