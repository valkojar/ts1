package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StandardItemTest {
    StandardItem standardItem;

    int INPUT_ID_1 = 595;
    int INPUT_ID_2 = 569;

    String INPUT_NAME_1 = "Samsung Galaxy A10";
    String INPUT_NAME_2 = "Sony Xperia T";

    float INPUT_PRICE_1 = 3290;
    float INPUT_PRICE_2 = 1815;

    String INPUT_CATEGORY_1 = "Electronics";
    String INPUT_CATEGORY_2 = "Electronics";

    int INPUT_LOYALTY_1 = 5;
    int INPUT_LOYALTY_2 = 3;

    @Test
    public void ConstructorTest(){
        standardItem = new StandardItem(INPUT_ID_1, INPUT_NAME_1, INPUT_PRICE_1, INPUT_CATEGORY_1, INPUT_LOYALTY_1);

        Assertions.assertEquals(INPUT_ID_1, standardItem.getID());
        Assertions.assertEquals(INPUT_NAME_1, standardItem.getName());
        Assertions.assertEquals(INPUT_PRICE_1, standardItem.getPrice());
        Assertions.assertEquals(INPUT_CATEGORY_1, standardItem.getCategory());
        Assertions.assertEquals(INPUT_LOYALTY_1, standardItem.getLoyaltyPoints());
    }

    @Test
    public void CopyTest(){
        standardItem = new StandardItem(INPUT_ID_2, INPUT_NAME_2, INPUT_PRICE_2, INPUT_CATEGORY_2, INPUT_LOYALTY_2);

        StandardItem standardItemCopy = standardItem.copy();

        Assertions.assertEquals(standardItem, standardItemCopy);
    }

    @ParameterizedTest(name = "Two standard items with same inputs should equal to one another")
    @CsvSource({"570, Samsung Galaxy Note 3, 3295, Electronics, 3"})
    public void EqualsTest(int input, String name, float price, String category, int loyaltyPoints){
        standardItem = new StandardItem(input, name, price, category, loyaltyPoints);
        StandardItem standardItemSameInputs = new StandardItem(input, name, price, category, loyaltyPoints);

        Assertions.assertEquals(true, standardItem.equals(standardItemSameInputs));
    }
}
