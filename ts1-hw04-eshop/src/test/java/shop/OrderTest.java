package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class OrderTest {
    Order order;
    ArrayList<Item> itemsInput;
    ShoppingCart cartInput;

    int INPUT_ID_1 = 595;
    int INPUT_ID_2 = 569;

    String INPUT_NAME_1 = "Samsung Galaxy A10";
    String INPUT_NAME_2 = "Sony Xperia T";

    float INPUT_PRICE_1 = 3290;
    float INPUT_PRICE_2 = 1815;

    String INPUT_CATEGORY_1 = "Electronics";
    String INPUT_CATEGORY_2 = "Electronics";

    int INPUT_LOYALTY_1 = 5;
    int INPUT_LOYALTY_2 = 3;

    String INPUT_CUSTOMERNAME = "Jaroslav Valkoun";
    String INPUT_CUSTOMERADDRESS = "Klokočná 101";
    int INPUT_STATE = 1;

    @BeforeEach
    public void ShoppingCartSetup(){
        itemsInput = new ArrayList<Item>();

        itemsInput.add(new StandardItem(INPUT_ID_1, INPUT_NAME_1, INPUT_PRICE_1, INPUT_CATEGORY_1, INPUT_LOYALTY_1));
        itemsInput.add(new StandardItem(INPUT_ID_2, INPUT_NAME_2, INPUT_PRICE_2, INPUT_CATEGORY_2, INPUT_LOYALTY_2));

        cartInput = new ShoppingCart(itemsInput);
    }

    @Test
    public void ConstructorTestWithState(){
        order = new Order(cartInput, INPUT_CUSTOMERNAME, INPUT_CUSTOMERADDRESS, INPUT_STATE);

        Assertions.assertEquals(cartInput.getCartItems(), order.getItems());
        Assertions.assertEquals(INPUT_CUSTOMERNAME, order.getCustomerName());
        Assertions.assertEquals(INPUT_CUSTOMERADDRESS, order.getCustomerAddress());
        Assertions.assertEquals(INPUT_STATE, order.getState());
    }

    @Test
    public void ConstructorTestWithoutState(){
        order = new Order(cartInput, INPUT_CUSTOMERNAME, INPUT_CUSTOMERADDRESS);

        Assertions.assertEquals(cartInput.getCartItems(), order.getItems());
        Assertions.assertEquals(INPUT_CUSTOMERNAME, order.getCustomerName());
        Assertions.assertEquals(INPUT_CUSTOMERADDRESS, order.getCustomerAddress());
    }
}
