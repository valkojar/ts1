package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;

import java.util.ArrayList;

public class EShopControllerTest {
    EShopController eShopController;
    ShoppingCart cart;

    int[] ITEM_COUNT = {10,10,4,5,10,2};

    Item[] STORAGE_ITEMS = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
            new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
            new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
            new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
    };

    @BeforeEach
    public void setup(){
        eShopController = new EShopController();
        eShopController.startEShop();

        eShopController.insertItemsIntoStorage(STORAGE_ITEMS, ITEM_COUNT);
    }

    @Test
    public void EmptyCartPurchaseTest() throws NoItemInStorage {
        cart = new ShoppingCart();
        Assertions.assertEquals(false,
                eShopController.purchaseShoppingCart(cart, "Jarmila Novakova", "Spojovaci 23, Praha 3"));
    }

    @Test
    public void AddingItemsToCartTest(){
        cart = new ShoppingCart();
        cart.addItem(STORAGE_ITEMS[3]);
        cart.addItem(STORAGE_ITEMS[4]);
        cart.addItem(STORAGE_ITEMS[5]);

        Assertions.assertEquals(STORAGE_ITEMS[3], cart.getSingleCartItem(0));
    }

    @Test
    public void RemovingItemsFromCartTest(){
        cart = new ShoppingCart();
        cart.addItem(STORAGE_ITEMS[3]);
        cart.addItem(STORAGE_ITEMS[4]);
        cart.addItem(STORAGE_ITEMS[5]);

        cart.removeItem(4);
        cart.removeItem(5);

        ArrayList<Item> expectedItems = new ArrayList<>();
        expectedItems.add(STORAGE_ITEMS[3]);

        Assertions.assertEquals(expectedItems, cart.getCartItems());
    }

    @Test
    public void FilledCartPurchaseTest() throws NoItemInStorage {
        cart = new ShoppingCart();
        cart.addItem(STORAGE_ITEMS[0]);
        cart.addItem(STORAGE_ITEMS[1]);
        cart.addItem(STORAGE_ITEMS[2]);

        eShopController.purchaseShoppingCart(cart, "Libuse Novakova","Kosmonautu 25, Praha 8");

        eShopController.archive.printItemPurchaseStatistics();
        eShopController.storage.printListOfStoredItems();
    }
}
