package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

public class ItemStockTest {

    ItemStock itemStock;
    StandardItem refItemInput;

    int INPUT_ID = 595;
    String INPUT_NAME = "Samsung Galaxy A10";
    float INPUT_PRICE = 3290;
    String INPUT_CATEGORY = "Electronics";
    int INPUT_LOYALTY = 5;

    @BeforeEach
    public void setup(){
        refItemInput = new StandardItem(INPUT_ID, INPUT_NAME, INPUT_PRICE, INPUT_CATEGORY, INPUT_LOYALTY);
    }

    @Test
    public void ConstructorTest(){
        itemStock = new ItemStock(refItemInput);

        Assertions.assertEquals(refItemInput, itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());

    }

    @ParameterizedTest(name = "Increasing the item count by {2}, should be equal to {2}")
    @CsvSource({"2, 2", "4, 4"})
    public void IncreaseTest(int countIncrease, int expectedResult){
        itemStock = new ItemStock(refItemInput);

        itemStock.IncreaseItemCount(countIncrease);

        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }

    @ParameterizedTest(name = "Decreasing the item count by {2}, should be equal to {2}")
    @CsvSource({"2, -2", "4, -4"})
    public void DecreaseTest(int countDecrease, int expectedResult){
        itemStock = new ItemStock(refItemInput);

        itemStock.decreaseItemCount(countDecrease);

        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }
}
