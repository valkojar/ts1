package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

public class CalcTests {

    Calc calc;

    @BeforeEach
    public void setup(){
        calc = new Calc();
    }

    @Test
    public void Add_TwoPlusFour_Six(){
        int a = 2;
        int b = 4;
        int expectedResult = 6;

        int result = calc.add(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Subtract_FiveSubtractedFromTen_Five(){
        int a = 10;
        int b = 5;
        int expectedResult = 5;

        int result = calc.subtract(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Multiply_FourMultipliedByFive_Twenty(){
        int a = 4;
        int b = 5;
        int expectedResult = 20;

        int result = calc.multiply(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwentyDividedByFour_Five(){
        int a = 20;
        int b = 4;
        int expectedResult = 5;

        int result = calc.divide(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwoDividedByZero_ExceptionThrow(){
        int a = 2;
        int b = 0;

        Assertions.assertThrows(Exception.class, () -> calc.divide(a, b));
    }

    @AfterEach
    public void closeEach(){

    }

}
