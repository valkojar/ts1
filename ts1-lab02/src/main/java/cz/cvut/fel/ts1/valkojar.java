package cz.cvut.fel.ts1;

public class valkojar {
    public long factorial(int n){
        int result = 1;
        int i = 1;
        //while cycle going from i to n
        //result gets multiplied over and over again, i counts up
        while (i <= n){
            result = result * i;
            i = i + 1;
        }

        return result;
    }
}
