package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class valkojarTest {
    @Test
    public void factorialTest(){
        valkojar test = new valkojar();
        int n = 5;
        long expectedResult = 120;

        long result = test.factorial(n);

        //compare the expected result and the actual result
        Assertions.assertEquals(expectedResult, result);
    }
}
